var brands = [{
    element: document.getElementById("curious-contraptions"),
    left: 5,
    top: 0,
    speedX: 0,
    speedY: 0,
    url: 'http://curious-contraptions.com'
  },
  {
    element: document.getElementById("jam-outside"),
    left: 5,
    top: 0,
    speedX: 0,
    speedY: 0,
    url: 'http://jamoutside.com'
  }
]
var myInterval = null;
function initPage() {
  var a = document.getElementById("jam-outside")
  a.style.color = "red"
  console.log(a)
  console.log('init')
  console.log(brands)
  brands[0].element = document.getElementById("curious-contraptions");
  brands[1].element = document.getElementById("jam-outside");
  var brandAreaTop = document.getElementById("brand-area-box").offsetTop;
  console.log(brandAreaTop)
  brands[0].top = brandAreaTop + 15;
  brands[0].element.style.top = parseInt(brands[0].top) + 'px';
  brands[1].top = brandAreaTop + brands[0].element.offsetHeight + 20;
  brands[1].element.style.top = parseInt(brands[1].top) + 'px';
  myInterval = setInterval(updatePosition, 10);
}

var pos = 5;

function updatePosition() {
  var brandAreaTop = document.getElementById("brand-area-box").offsetTop;
  var brandAreaBottom = brandAreaTop + document.getElementById("brand-area-box").offsetHeight
  var brandAreaRight = document.getElementById("brand-area-box").offsetWidth
  for (brand in brands) {

    var element = brands[brand].element;
    var elementBottom = element.offsetTop + element.offsetHeight;
    brands[brand].left += brands[brand].speedX
    brands[brand].top += brands[brand].speedY
    if (brands[brand].left <= 0) {
      brands[brand].left = 0
      brands[brand].speedX = 0;
    }
    if (brands[brand].top <= brandAreaTop + 3) {
      brands[brand].top = brandAreaTop + 3
      brands[brand].speedY = 0;
    }
    brands[brand].element.style.left = parseInt(brands[brand].left) + 'px';
    brands[brand].element.style.top = parseInt(brands[brand].top) + 'px';

    if (elementBottom > brandAreaBottom - 3) {
      brands[brand].top = brandAreaBottom - element.offsetHeight - 3
      brands[brand].speedY = 0;
    }
    if (brands[brand].left + element.offsetWidth > brandAreaRight) {
      brands[brand].left = brandAreaRight - element.offsetWidth
      brands[brand].speedX = 0;
    }

    checkGoal(brands[brand].top, brands[brand].left, brands[brand].left + element.offsetWidth, brands[brand].top + element.offsetHeight, brands[brand].url);
  }
}
function checkGoal(top, left, right, bottom, url){
  var visitBrandBox = document.getElementById("visit-brand-box")
  var upperLimit = visitBrandBox.offsetTop ;
  var lowerLimit = visitBrandBox.offsetTop + visitBrandBox.offsetHeight;
  var leftLimit = visitBrandBox.offsetLeft;
  // console.log(upperLimit, leftLimit, lowerLimit)
  // console.log(top,left,bottom)
  if(!!window.performance && window.performance.navigation.type === 2)
{
    window.location.reload();
}

  if (top > upperLimit && bottom < lowerLimit && left > leftLimit) {


    clearInterval(myInterval);
    // initPage();
    window.location.href = url;

    // initPage();
    // window.location.replace(url);
    setInterval(myInterval);

    // alert("Would you like to navigate?!")

    // console.log('goal')
  }
}

function moveUp(brandIndex) {
  brands[brandIndex].speedY -= 1;
}

function moveDown(brandIndex) {
  brands[brandIndex].speedY += 1;
}

function moveLeft(brandIndex) {
  brands[brandIndex].speedX -= 1;
}

function moveRight(brandIndex) {
  brands[brandIndex].speedX += 1;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function submitEmail(){
  var submittedEmail =document.getElementById('email').value
  if (submittedEmail == 'e@mail.com')
  {
    emailAlert();
  }
  else if(validateEmail(submittedEmail)){
    document.getElementById('email-alert').innerHTML="Welcome to the mailing list!"
    document.getElementById('email-alert').style.display="inline"
    document.getElementById('email-alert').style.color="green"

  }
  else{
    emailAlert();
  }

}
function emailAlert(){
  document.getElementById('email-alert').innerHTML="Please enter a valid email address"
  document.getElementById('email-alert').style.display="inline"
  document.getElementById('email-alert').style.color="red"

}
window.onpageshow = function(event) {
    if (event.persisted) {
        window.location.reload()
    }
};
